#! /bin/bash
#Marc Cullell M01
#Febrer 2024
#Exemples case
#.....................................

#exemple dies setmana

case $1 in
	"dl"|"dm"|"dx"|"dj"|"dv")
		echo "$1 és laborable"
		;;
	"ds"|"dg")
		echo "$1 és festiu"
		;;
	*)
		echo "$1 no és vàlid. argument hauria de ser un dels següents valors: dl, dm, dx, dj, dv, ds, dg"
		;;
esac
exit 0
#exemple vocals
case $1 in
	[aeiou])
		echo "$1 és una vocal"
		;;
	[b-z])
		echo "$1 és una consonant"
		;;

	*)
		echo "$1 és una altra cosa"
		;;
esac
exit 0
#
