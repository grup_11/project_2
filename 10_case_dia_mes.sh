#! /bin/bash
#Marc Cullell
#Febrer 2024
#Descripció di els dies que té un mes
#Synopsis: prog mes
#	a) validar rep 1 <= arg
#	b) validar mes [1-12]
#	c) programa
#.................................................
#Variables d'error:
ERR_NARGS=1
ERR_MES=2
#
#
#validar rep 1 <= arg
#
if [ $# -ne 1 ]
then
	echo "Error: el programa ha de només rep 1 argument"
	echo "Usage: $0 mes"
	exit $ERR_NARGS
fi

#validar mes [1-12]

if [ $1 -lt 1 -o $1 -gt 12 ]
then
		echo "Error: $1 no és un número de 1 a 12"
		echo "Usage: $0 mes(1-12)"
		exit $ERR_MES
fi

#programa
case $1 in
	"1"|"3"|"5"|"7"|"8"|"10"|"12")
		echo "el mes $1 té 31 dies";;
	[2])
		echo "el mes $1 té 28 o 29 dies";;
	*)
		echo "el mes $1 té 30 dies";;

esac
exit 0
