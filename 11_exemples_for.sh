#! /bin/bash
#Marc Cullell
#Febrer 2024
#Descripció exemples bucle for
#.................

#8 llistar tots el logins ordenats i numerats
#
llistat=$(cut -d: -f1 /etc/passwd | sort)
num=1
for element in $llistat
do
	echo "$num $element"
	((num++))
done
exit 0



#7) llistar numerant les línees
#
#
#
llistat=$(ls)
num=1
for entrada in $llistat
do
	echo "$num $entrada"
	((num++))
done
exit 0



#6) iterar cada un dels valors d'executar l'ordre "ls"
#
llistat=$(ls)
for entrada in $llistat
do
	echo "$entrada"
done
exit 0



#5) numerar arguments

contador=1
for arg in $*
do
  echo "$num: $arg"
  num=(($num+1))
  #((num++))
done
exit 0
#
#
#4) $@ expandeix, $* no expandeix
#
for arg in "$@"
do
  echo "$arg"
done
exit 0
#3)iterar per la llista d'arguments
#
for arg in $*
do
  echo "$arg"
done
exit 0
#
#1)iterar noms
for nom in "pere" "marta" "anna" "pau"
do
	echo "$nom"
done
exit 0
