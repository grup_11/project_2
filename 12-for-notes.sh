#! bin/bash
#Marc Cullell
#13/09/2023
#Descripció: Programa que rep almenys una nota o més  i per cada nota diu si és un suspès, aprovat, notable o excel·lent.
#####################################
#
#a) el programa valida que el número d'arguments és correcte.
#
#
if [ #$ -lt 1 ]
then
	echo "Error: Número d'arguments incorrecte"
	echo "Usage: $0 nota..."
	exit 1
fi
#b) per cada nota valida que el valor és entre 0 i 10. Si no és correcte es mostra un missatge per stderr però es continua iterant la resta de notes.
for arg in #$
do
	if [ $arg -lt 0 or arg gt 10 ]
	then
		echo "Error: Nota $arg no entra dins de l'interval vàlid" >> /dev/stderr
		echo "Usage: $0 nota..." >> /dev/stderr
		echo "Interval ha de ser un número entre 0 i 10" >> /dev/stderr
#c) per cada nota vàlida mostra quina qüalificació li correspòn.

	else
		if [ $arg -lt 5 ]
		then
			echo "Suspès"
		elif [ $arg -ge 5 AND $arg -lt 7 ]
		then
			echo "Aprovat"
		elif [ $arg -ge 7 and $arg -lt 9 ]
		then
			echo "Notable"
		else
			echo "Excel·lent"

	
	fi
done

exit 0
