#! /bin/bash
#Marc Cullell
#14/02/2024
#Descripció: Exemples while
##########################################
#
#
#
#
#
#
#8) processar linia a linia un fitxer numerant les linies i passant-les a majúscules
filein=$1
numeracio=1
while read -r line
do 
	chars=$(echo $line | wc -c)
	echo "$numeracio)($chars) $line" | tr 'a-z' 'A-Z'
	((numeracio++))
done < $filein
exit 0
#
#
#
#7) numerar les línees i mostrar en majúscula
numeracio=1
while read -r line

do
	echo "$numeracio) $line" | tr 'a-z' 'A-Z'
	((numeracio++))
done
exit 0

#
#6) Itera linia a linia fins a token
TOKEN="FI"
read -r line
numeracio=1
while [ "$line" != $TOKEN ]
do
	echo "$numeracio)$line"
	read -r line
	((numeracio++))
done
exit 0


#
#
#
#
#5) numerar les línees rebudes
comptador=1
while read -r line
do
	echo "$comptador) $line"
	((comptador++))
done
exit 0

#
#
#
#4) Processar stdin línea a línea
while read -r line
do
	echo "$line"
done
exit 0


#3) Iterar la llista d'arguments 
while [ -n "$1" ]
do
	echo "$1 $# $*"
	shift
done
exit 0
#
#2) Comptador decrementa valor rebut
comptador=$1
while [ $comptador -ge 0 ]
do
	echo "$comptador"
	((comptador--))
done



exit 0
#1) Mostrar números de 1 a 10
MAX=10
num=1
while [ $num -le $MAX ]
do
	echo "$num "
	((num++))	
done
#
exit 0
