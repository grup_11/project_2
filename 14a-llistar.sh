#! /bin/bash
#@marc.culle
#febrer 2024
#prog dir
#rep un arg que és directori i es llista
#......................................................
#1)Validar que hi ha un argument
#
if [ $# -lt 1 ]
then
	echo "Error: Número d'arguments < 0"
       	echo "Usage: $0 arguments"
	exit 1
fi

#2) Validar que és un dir
if [ ! -d $1  ]
then
	echo "Error: $1 No és un directori"
	echo "Usage: $0 dir"
	exit 2
fi
#3)programa: llistar
ls $1

exit 0
