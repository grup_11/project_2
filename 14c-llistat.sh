#! /bin/bash
#@marc.culle
#febrer 2024
#prog dir
#a)rep un arg que és directori i es llista
#b)llistar numerant els elements del dir
#c)dir a més si cada element és un directori, regular file o altre.
#......................................................
#
#
#1)Validar que hi ha un argument
#
if [ $# -lt 1 ]
then
	echo "Error: Número d'arguments < 0"
       	echo "Usage: $0 arguments"
	exit 1
fi

#2) Validar que és un dir
if [ ! -d $1  ]
then
	echo "Error: $1 No és un directori"
	echo "Usage: $0 dir"
	exit 2
fi
#3)programa: llistar

llistat=$(ls $1)
numeracio=1
for element in $llistat
do
	if [ -d $1$element ]
	then
		tipus_file="dir"
	elif [ -f $1/$element ]
	then
		tipus_file="dir"
	else
		tipus_file="altre"
	fi
	echo "$numeracio) $element $tipus_file"
	((numeracio++))
done
exit 0
