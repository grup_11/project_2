#! /bin/bash
#Marc Cullell
#Febrer 2024
#Exercicis d'scripts
#------------------------------------------
#
##10. Fer un programa que rep com a argument un número indicatiu del número màxim de línies a mostrar. El programa processa stdin línia a línia i mostra numerades un màxim de num línies.
#
##9. Fer un programa que rep per stdin noms d’usuari (un per línia), si existeixen en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per stderr.

#
###8. Fer un programa que rep com a argument noms d’usuari, si existeixen en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per stderr.
#
##7. Processar línia a línia l’entrada estàndard, si la línia té més de 60 caràcters la mostra, si no no.

#6. Fer un programa que rep com a arguments noms de dies de la setmana i mostra quants dies eren laborables i quants festius. Si l’argument no és un dia de la setmana genera un error per stderr. Exemple: $ prog dilluns divendres dilluns dimarts kakota dissabte sunday

dies_setmana=$@
laborables=0
festius=0
for dia in $dies_setmana
do
	case $dia in 
		"dilluns" | "dimarts" | "dimecres" | "dijous" | "divendres" )
			echo $dia
			((laborables++))
		"dissabte" | "diumenge" )
			echo $dia
			((festius++))
		*)
			echo "Error: l'argument $dia no és vàlid."
			echo "Usage: $0 (dilluns o dimarts o dimecres o dijous o divendres o dissabte o diumenge)."
	esac
done
echo "laborables: $laborables"
echo "festius: $festius"

exit 0
##5. Mostrar línia a línia l’entrada estàndard, retallant només els primers 50 caràcters.

#Entenc que vol mostrar els primers 50 caràcters
MAX=50
while read -r line
do
	echo $line | cut -c 1-$MAX
done

exit 0

#4. Fer un programa que rep com a arguments números de mes (un o més) i indica per a cada mes rebut quants dies té el més.

for arg in $@
do

	if [ $arg -lt 0 -o $arg -gt 12 ]
	then
		echo "Error: valor de l'argument ( $arg ) no és vàlid" 
		echo "Usage: l'argument hauria de ser un número de 1 a 12"
		exit 1
	fi
	case $arg in 
		"1"|"3"|"5"|"7"|"8"|"10"|"12")
		echo "número de dies del mes número $arg és 31";;
		"4"|"6"|"9"|"11") 
		echo "número de dies del mes número $arg és 30";;
		*)
		echo "número de dies del mes número $arg és 28 o 29";;
	esac

done
exit 0
#-#3. Fer un comptador des de zero fins al valor indicat per l’argument rebut.
comptador=0
MAX=$1
while [ $comptador -le $MAX ]
do
	echo "$comptador"
	((comptador++))
done
	
exit 0
#
#
#
#
#
##2. Mostar els arguments rebuts línia a línia, tot numerànt-los
#
numeracio=1
for arg in $@
do
	echo "$numeracio) $arg"
	((numeracio++))
	
done
exit 0
#1.Mostrar l'entrada estàndard numerant línea a línea

#numeracio=1
#while read -r line
#do
#	echo "$numeracio) $line"
#	((numeracio++))
#done
#exit 0




