#! /bin/bash
#marc.culle
#febrer 2024
#Descripció: Exercicis Scripts 
################################################
#Processar arguments
#
#7) Programa: prog -f|-d arg1 arg2 arg3 arg4
#a) Valida que els quatre arguments rebuts són tots del tipus que indica el flag. És a dir, si es crida amb -f valida que tots quatre són file. Si es crida amb -d valida que tots quatre són directoris. Retorna 0 ok, 1 error no args, 2 hi ha elements errònis. Exemple: prog -f carta.txt a.txt /tmp fi.txt → retorna status 2.
#b) Ampliar amb el cas: prog -h|--help.

#6) Processar per stdin línies d’entrada tipus “Tom Snyder” i mostrar per stdout la línia en format → T. Snyder. Processar files/directoris
while read -r line
do
nom=$(echo $line | cut -c1)
#inicial=$(echo $nom | cut -c1)
#cognom=$(echo $nom | cut -d' ' -f2)
done
exit 0
#5) Processar stdin mostrant per stdout les línies de menys de 50 caràcters.
#
while read -r line
do
# "^.{,50}$"
done
exit 0
#
#
#4) Processar stdin cmostrant per stdout les línies numerades i en majúscules..
#
#
#
exit 0
##3) Processar arguments que són matricules:
#a) Llistar les vàlides, del tipus: 9999-AAA.
comptador=0
for matricula in $@
do
	echo "$matricul" | grep	"^[0-9]{4} [A-Z]{3}$"
	if [ $? -ne 0 ]
	then
		echo "error"
		((comptador++))
	fi
done
exit 0
#b) stdout les que són vàlides, per stderr les no vàlides. Retorna de status el número d’errors (de no vàlides). Processar stdin
#1) Processar els arguments i mostrar per stdout només els de 4 o més caràcters.

#
exit 0
for arg in $*
do
	echo -e "$arg{4}"
done
exit 0
#2) Processar els arguments i comptar quantes n’hi ha de 3 o més caràcters.




8) Programa: prog file...
a)Validar existeix almenys un file. Per a cada file comprimir-lo. Generar per stdout
el nom del file comprimit si s’ha comprimit correctament, o un missatge d’error per
stderror si no s’ha pogut comprimir. En finalitzar es mostra per stdout quants files
ha comprimit.
Retorna status 0 ok, 1 error no args, 2 si algun error en comprimir.
b) Ampliar amb el cas: prog -h|--help.
9) Programa: prog.sh [ -r -m -c cognom -j -e edat ] arg...
Escriure el codi que desa en les variables: opcions, cognom, edat i arguments els valors
corresponents.
No cal validar ni mostrar res!
Per exemple si es crida: $ prog.sh -e 18 -r -c puig -j wheel postgres ldap
retona: opcions «-r -j», cognom «puig», edat «18», arguments «wheel postgres ldap»

Processar passwd
10)Programa: prog.sh
Rep per stdin GIDs i llista per stdout la informació de cada un d’aquests grups, en
format: gname: GNAME, gid: GID, users: USERS
11) Idem però rep els GIDs com a arguments de la línia d’ordres.
12) Programa -h uid...
Per a cada uid mostra la informació de l’usuari en format:
logid(uid) gname home shell#
