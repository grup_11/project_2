#! /bin/bash
#
#
#
#programa que processa el contingut d'un fitxer que són matricules am b format AAAA999
#
#
#validar num arg
#

#
#programa
error=0
while read -r line
do
	matricula=$line
	echo $matricula | grep -E "^[A-Z]{4}[0-9]{3}$"
	if [$? -ne 0 ]; then
		echo "Error stolen: $line" >> /dev/stderr
		error=3
	fi
done < $1
exit $error
#
